# express-mysql

1) Создать таблици (**Employes**, **Departments**, **Tasks**). Файл с **sql** скриптами сохранить в папке репозитория. 


Модель **Employe**:
```
  {
    id: number,
    firstName: string,
    lastName: string,
    age: number,
    salary: number,
    createdAt: DATE,
  }
```

Модель **Department**:
```
  {
    id: number,
    name: string,
    isActive: boolean,
    createdAt: DATE,
  }
```

Модель **Task**:
```
  {
    id: number,
    name: string,
    startDate: DATE,
    endDate: DATE,
    createdAt: DATE
  }
```

2) Реализовать базовые **CRUD** операции для каждой сущности

3) Дополнительно реализовать следующее api

3.1) Поиск рааботников по имени. Возвращает список сотрудников.
```
  GET /employes?search='someString'
```
3.2) Получить список работников в диапазоне возрастов. Возвращает список сотрудников.
```
  GET /employes?minYear=number&maxYear=number
```
3.3) Получить общую сумму всех зарплат
```
  GET /employes/total-salary
```
3.4)  Получить список активных департаментов. Возвращает список департаментов
```
  GET /departments?isActive=true
```

